# Projekt Sterownika GPC

## Wstęp

Ten projekt jest implementacją sterownika GPC (Generalized Predictive Control) - uogólnionego sterownika predykcyjnego. Sterownik predykcyjny to rodzaj sterownika, który korzysta z modelu systemu do przewidywania przyszłych wyników sterowania. Wykorzystuje te przewidywania, aby zoptymalizować sterowanie w taki sposób, aby zminimalizować różnicę między przewidywanymi wyjściami a żądanymi wyjściami.

## Wymagania

Projekt został napisany w języku Python, więc wymagany jest interpreter Pythona. Dodatkowo wymagane są następujące biblioteki:
- cvxopt
- scipy
- numpy
- matplotlib

## Opis kodu

### Definiowanie stałych i parametrów

W początkowej części kodu definiowane są różne stałe i parametry, takie jak siła grawitacji (g), gęstość ro (ro), maksymalna liczba iteracji predykcji (MaxIterationPrediction) i ograniczenia sterowania (u_min, u_max, du_min, du_max).

### Modelowanie systemu

W kolejnej części kodu, system jest modelowany przy użyciu równań stanu. Używane są do tego macierze A, B, C, D. Również wyznaczane są bieguny systemu.

### Symulacja odpowiedzi skokowej

Kod symuluje odpowiedź skokową systemu zarówno dla modelu ciągłego, jak i dyskretnego. Wykorzystuje do tego transmitancje i oblicza czas, w którym system osiąga 95% wartości.

### Wyliczanie wielomianów 

Dla modelu dyskretnego kod wylicza wielomiany A(q^-1) oraz B(q^-1).

### Sterownik GPC

Reszta kodu to implementacja sterownika GPC. Funkcje `StateCalcFromDisc` i `PredictionVectors` są używane do wyliczania stanu i wektorów predykcyjnych. W funkcji `PredictionictionParameters` wyznaczane są macierze niezbędne do wyliczenia sterowania predykcyjnego. Optymalne sterowanie jest wyznaczane za pomocą metody QP (quadratic programming).

## Użycie

Uruchomienie kodu spowoduje przeprowadzenie symulacji systemu i wyznaczenie optymalnego sterowania. Wyjście kodu zawiera różne dane i wyniki, takie jak bieguny systemu, transmitancje, czas dla 95% wartości, wielomiany A(q^-1) i B(q^-1) oraz wyniki sterowania GPC. Wszystkie wyniki są wyświetlane w konsoli.
