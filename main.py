# Projekt sterownika GPC

import cvxopt as opt
from scipy.signal import cont2discrete
from scipy import signal
import numpy as np
import matplotlib.pyplot as plt
import math

# Radosław Pawelec 175631 a=1
# Patryk Martyniak 181578 b=8

a = 1
b = 8

# Dane do symulacji
g = 10.0  # m/s^2
ro = 1000.0  # kg/m^3
alpha1 = 0.000001  # m^3/(sPa)
alpha2 = 0.000001  # m^3/(sPa)
alpha = alpha1
S1 = 1 + b  # m^2
S2 = 0.5 + a  # m^2
# Maksymalna ilosc iteracji predykcji
MaxIterationPrediction = 5
# Staly iloczyn parametrow
R = g * ro * alpha

# Ograniczenia
u_min = 0.0
u_max = 3.0
du_min = -2.0
du_max = 2.0

# Model w dziedzinie czasu ciągłego oraz zmienne stanu
A = np.array([[0, 1], [-1 / 135000, -1950 / 135000]])
B = np.array([[0], [1]])
C = np.array([[1 / 135000, 0]])
D = np.array([[0]])

# Licznik transmitancji wyznaczony z ukladu rownan (w opracowaniu)
num = 1 / 135000

# Wielomian w mianowniku transmitancji wyznaczony z ukladu rownan (w opracowaniu)
# s^2 + 1950/135000 s + 1/135000

# Wyznaczenie biegunow i zapis mianownika w formie iloczynowej
a = 1
b = 1950 / 135000
c = 1 / 135000
delta = b * b - 4 * a * c
s1 = (-b - np.sqrt(delta)) / 2 * a
s2 = (-b + np.sqrt(delta)) / 2 * a

# Biegun 1
print("Biegun 1:", s1)
p1 = np.poly1d([1, -s1])
# Biegun 2
print("Biegun 2:", s2)
p2 = np.poly1d([1, -s2])
den = p1 * p2

# Transmitancja w dziedzinie czasu ciaglego
Continous_tf = signal.TransferFunction(num, den)
print(Continous_tf)

# Odpowiedz skokowa w dziedzinie czasu ciąglego
ContStepT, ContStepY = signal.step(Continous_tf)
u = np.ones(len(ContStepT))

# Iteracyjne wyznaczanie czasu osiagniecia 95% wartosci przez odpowiedz skokowa
i = 0
for i, value in enumerate(ContStepT):
    if ContStepT[i] >= 0.95 * ContStepT[-1]:
        Ts95 = value
        break
    i = i + 1
print("Czas dla 95% wartości:", Ts95)

# Czas próbkowania
Ts = 650

# Model dyskretny

# Zero w nieskończoności
zeroInf = -1

# Odwzorowanie biegunów
z1 = math.exp(s1 * Ts)
z2 = math.exp(s2 * Ts)
print(f"z1: {z1}\nz2: {z2}")

# Obliczenie wzmocnienia K
K = (1 - z1 - z2 + z1 * z2) / (1 - zeroInf)
num = K * np.poly1d([1, -zeroInf])
pd1 = np.poly1d([1, -z1])
pd2 = np.poly1d([1, -z2])
den = pd1 * pd2

# Transmitancja dyskretna
Disctf = cont2discrete((A, B, C, D), Ts)
DiscStepT, DiscStepY = signal.dstep(Disctf)

# Wyznaczenie wielomianów A(q^-1) oraz B(q^-1)

# Wyznaczenie równiań wielomianu Aq i Bq
Aq = np.zeros(len(np.asarray(den)) + 1)
Aq[:-1] = np.asarray(den)
Aq[1:] = Aq[1:] - Aq[:-1]

Bq = np.asarray(num)
Bq = Bq.reshape(Bq.shape[0])

print("Mianownik:", den)
print("Wielomian A(q^(-1)):", Aq)
print("Licznik:", num)
print("Wielomian B(q^(-1)):", Bq)

# Dane do regulatora GPC

# Dane do wyznaczania horyzontów
stepHy = 3
stepHu = 1
stepRo = 0.1
PredictionRo = 0.2
PredictionLambda = 0


def StateCalcFromDisc(x1, x2, u):
    x1_new = (z1 + z2) * x1 + (-z1 * z2) * x2 + u
    x2_new = x1
    y_new = K * x1_new + K * x2_new
    return x1_new, x2_new, y_new


# Funkcja ustawiająca wektory prekycyjne
def PredictionVectors(step, parametres):
    # step - krok parametru
    # parametres = 0 - dla wektorów regulatora
    # parametres = 1 - dla wektorów Hy, Hu, Ro
    global discretetimeLength
    discretetimeLength = len(DiscStepT)

    if (parametres == 1):
        step += 1
    yreference = np.ones((MaxIterationPrediction, discretetimeLength + MaxIterationPrediction * step))
    yPrediction = np.ones((MaxIterationPrediction, discretetimeLength + MaxIterationPrediction * step))
    y = np.zeros((MaxIterationPrediction, discretetimeLength + MaxIterationPrediction * step))
    u = np.zeros((MaxIterationPrediction, discretetimeLength + MaxIterationPrediction * step))
    du = np.zeros((MaxIterationPrediction, discretetimeLength + MaxIterationPrediction * step))
    x1state = np.zeros((MaxIterationPrediction, discretetimeLength + MaxIterationPrediction * step))
    x2state = np.zeros((MaxIterationPrediction, discretetimeLength + MaxIterationPrediction * step))
    i = 0
    while i < MaxIterationPrediction:
        yreference[i, 0] = 0
        i = i + 1
    return yreference, yPrediction, y, u, du, x1state, x2state


# Wektory dla Hy
yreferenceHy, yPredictionHy, yHy, uHy, duHy, x1stateHy, x2stateHy = PredictionVectors(stepHy, 1)
# Wektory dla Hu
yreferenceHu, yPredictionHu, yHu, uHu, duHu, x1stateHu, x2stateHu = PredictionVectors(stepHy, 2)
# Wektory dla Ro
yreferenceRo, yPredictionictionRo, yRo, uRo, duRo, x1stateRo, x2stateRo = PredictionVectors(stepHy, 2)
# Wektory dla sterownika bez ograniczeń
yreferenceControl, yPredictionControl, yControl, uControl, duControl, x1stateControl, x2stateControl = PredictionVectors(
    stepHy, 0)
# Wektory dla sterownika z ograniczeniami
yreferenceControlConst, yPredictionControlConst, yControlConst, uControlConst, duControlConst, x1stateControlConst, x2stateControlConst = PredictionVectors(
    stepHy, 0)


# Wyznaczanie macierzy H, P1 oraz P2

def PredictionictionParameters(Hy, Hu, step, setParametres, x1State, x2State, du, u, y, Ro, yPrediction):
    # Funkcja wyznacza macierze Ca, Cb, Ha, Hb oraz  optymalnege
    # Hy, Hu i Ro w zależności od wybranej setParametres: 1 > Hu, 2 > Hy, 3 > Ro, 4 > regulator

    global PredictionictionRo
    i = 0

    while i < MaxIterationPrediction:
        if setParametres == 1:
            step = stepHu
            PredictionHy = Hy
            PredictionHu = i * step + step
            PredictionictionRo = 0.1
        elif setParametres == 2:
            step = stepHy
            PredictionHu = Hu
            PredictionHy = i * step + step
            PredictionictionRo = 0.1
        elif setParametres == 3:
            step = stepRo
            PredictionHu = Hu
            PredictionHy = Hy
            PredictionictionRo = i * step + step
        elif setParametres >= 4:
            PredictionHu = Hu
            PredictionHy = Hy
            PredictionictionRo = Ro
            j = 1
        else:
            print("0 < setParametres < 5")

        Ca = np.zeros((PredictionHy, PredictionHy))
        offset = np.arange(PredictionHy)
        for j in range(Aq.shape[0]):
            offsetTemp = offset + j
            Ca[offsetTemp[offsetTemp < PredictionHy], offset[offsetTemp < PredictionHy]] = Aq[j]
        InvertCa = np.linalg.inv(Ca)

        offset = np.arange(PredictionHu)
        Cb = np.zeros((PredictionHy, PredictionHu))
        for j in range(Bq.shape[0]):
            offsetTemp = offset + j
            Cb[offsetTemp[offsetTemp < PredictionHy], offset[offsetTemp < PredictionHy]] = Bq[j]

        offset = np.arange(Aq.shape[0] - 1)
        Ha = np.zeros((PredictionHy, Aq.shape[0] - 1))
        for j in range(Aq.shape[0] - 1):
            offsetTemp = - offset + j
            Ha[offsetTemp[offsetTemp >= 0], offset[offsetTemp >= 0]] = Aq[j + 1]

        offsetTemp = np.arange(Bq.shape[0] - 1)
        Hb = np.zeros((PredictionHy, Bq.shape[0] - 1))
        for j in range(Bq.shape[0] - 1):
            offsetTemp = - offset + j
            Hb[offsetTemp[offsetTemp >= 0], offset[offsetTemp >= 0]] = Bq[j + 1]
        if setParametres >= 4:
            j = 1
        H = InvertCa @ Cb
        P1 = (-1) * (InvertCa @ Ha)
        P2 = (InvertCa @ Hb)

        offset = np.arange(1, PredictionHy + 1)
        yPrediction = yreferenceHy[i, j + offset] - (yreferenceHy[i, j] - y[i, j]) * pow(PredictionLambda, offset)
        offset = np.arange((-1) * P1.shape[1] + 1, 1)
        offset = offset + j
        Sum = np.sum(offset >= 0)
        yPast = np.zeros(P1.shape[1])
        yPast[:Sum] = np.flip(y[i, offset[offset >= 0]])

        offset = np.arange((-1) * P2.shape[1], 0)
        offset += j
        Sum = np.sum(offset >= 0)
        duPast = np.zeros(P2.shape[1])
        duPast[:Sum] = np.flip(du[i, offset[offset >= 0]])

        W = 2 * ((np.transpose(H) @ H) + PredictionictionRo * np.identity(H.shape[1]))
        V = (-2) * (np.transpose(H) @ (yPrediction - (P1 @ yPast) - (P2 @ duPast)))

        if setParametres == 5:
            Edu = np.concatenate([np.identity(PredictionHu), (-1) * np.identity(PredictionHu)])
            Fdu = np.concatenate([np.full(PredictionHu, du_max), (-1) * np.full(PredictionHu, du_min)])
            MT = np.tril(np.ones((PredictionHu, PredictionHu)))
            Eu = np.concatenate([MT, (-1) * MT])
            Fu = np.concatenate(
                [np.full(PredictionHu, u_max - u[j - 1][j - 1]), np.full(PredictionHu, -u_min + u[j - 1][j - 1])])
            ERes = np.concatenate([Edu, Eu])
            FRes = np.transpose(np.atleast_2d(np.concatenate([Fdu, Fu])))
            OptimalPrediction = opt.solvers.qp(opt.matrix(W), opt.matrix(V), opt.matrix(ERes), opt.matrix(FRes))['x']
        else:
            OptimalPrediction = opt.solvers.qp(opt.matrix(W), opt.matrix(V))['x']
        j = 1
        for t in DiscStepT:
            if j <= PredictionHu:
                du[i, j] = OptimalPrediction[j - 1]
            else:
                du[i, j] = 0
            u[i, j] = u[i, j - 1] + du[i, j]
            x1State[i, j + 1], x2State[i, j + 1], y[i, j + 1] = StateCalcFromDisc(x1State[i, j], x2State[i, j], u[i, j])
            j = j + 1
        i = i + 1
    return du, u, y


# Optymalne Hu
duHu, uHu, yHu = PredictionictionParameters(Hy=12, Hu=0, Ro=0, step=stepHy, setParametres=1, x1State=x1stateHu,
                                            x2State=x2stateHu,
                                            du=duHu, u=uHu, y=yHu, yPrediction=yPredictionHu)
# Optymalne Hy
duHy, uHy, yHy = PredictionictionParameters(Hy=0, Hu=2, Ro=0, step=stepHy, setParametres=2, x1State=x1stateHy,
                                            x2State=x2stateHy,
                                            du=duHy, u=uHy, y=yHy, yPrediction=yPredictionHy)
# Optymalne Ro
duRo, uRo, yRo = PredictionictionParameters(Hy=12, Hu=8, Ro=0, step=stepRo, setParametres=3, x1State=x1stateRo,
                                            x2State=x2stateRo, du=duRo, u=uRo, y=yRo,
                                            yPrediction=yPredictionictionRo)

# Wyznaczanie regulatora z optymalnymi parametrami Hy, Hu, Ro
ControlHy = 16
ControlHu = 8
ControlRo = 0.01
duControl, uControl, yControl = PredictionictionParameters(Hy=ControlHy, Hu=ControlHu, Ro=ControlRo, setParametres=4,
                                                           x1State=x1stateControl,
                                                           x2State=x2stateControl, du=duControl, u=uControl, y=yControl,
                                                           step=1,
                                                           yPrediction=yPredictionControl)
duControlConst, uControlConst, yControlConst = PredictionictionParameters(Hy=ControlHy, Hu=ControlHu, Ro=ControlRo,
                                                                          setParametres=5,
                                                                          x1State=x1stateControlConst,
                                                                          x2State=x2stateControlConst,
                                                                          du=duControlConst, u=uControlConst,
                                                                          y=yControlConst,
                                                                          step=1,
                                                                          yPrediction=yPredictionControlConst)

# Wlasciwosci wykresow
plt.title("Charakterystyka porównawcza odpowiedzi skokowej dla dziedziny czasu ciągłego i dyskretnego")
plt.plot(ContStepT, u, 'b--', label="u(t)")
plt.plot(ContStepT, ContStepY, label="odpowiedź układu ciągłego", linewidth=4.0, color='g')
plt.plot(DiscStepT, np.squeeze(DiscStepY), label="odpowiedź układu dyskretnego", linewidth=4.0,
         color=(1.0, 1.0, 0.4, 1.0), linestyle='dotted')
plt.legend(loc='lower right')
plt.grid()
plt.rcParams['axes.facecolor'] = 'lightgrey'
plt.xlabel("Czas [s]")
plt.ylabel("y(t)")
plt.xlim([ContStepT[0], ContStepT[-1]])
plt.ylim([0, 1.1])


def ploting(yreference, y, u, du, step, legend, title, iterationPrediction):
    # yreference - wartosc y referencyjna; y - sygnały wyjściowe; u - sygnał sterujący; du - przyrost sygnałów sterujących
    # step - Krok; legend - legenda; title - tytuł; iterationPrediction - ilość iteracji

    fig, (ax1, ax2, ax3) = plt.subplots(3, 1)
    colors = (
    (0.0, 0.0, 0.0, 1.0), (1.0, 0.5, 0.0, 1.0), (0.0, 1.0, 0.0, 1.0), (1.0, 0.0, 0.0, 1.0), (0.0, 0.0, 1.0, 1.0))
    i = 0
    while i < iterationPrediction:
        plotColor = colors[i % 5]
        legenda = "{} = {}".format(legend, i * step + step)
        ax1.plot(DiscStepT, yreference[i, : discretetimeLength], color=plotColor, linestyle="dotted")
        ax1.plot(DiscStepT, y[i, : discretetimeLength], color=plotColor, linestyle="solid", label=legenda)
        ax2.step(DiscStepT, u[i, : discretetimeLength], color=plotColor, label=legenda)
        ax3.step(DiscStepT, du[i, : discretetimeLength], color=plotColor, label=legenda)
        i += 1
    plt.xlabel("Czas[s]")
    ax1.set_title(title)
    ax1.set_ylabel("sygnał wejściowy\nu(k)")
    ax2.set_ylabel("sygnał wyjściowy\ny(k)")
    ax3.set_ylabel("przyrosty sterowań\n∆u(k)")
    if iterationPrediction > 1:
        ax3.legend(loc='center left', bbox_to_anchor=(1.0, 0.5))
    ax1.grid()
    ax2.grid()
    ax3.grid()
    ax1.set_xlim([ContStepT[0], ContStepT[-stepHy * iterationPrediction]])
    ax2.set_xlim([ContStepT[0], ContStepT[-stepHy * iterationPrediction]])
    ax3.set_xlim([ContStepT[0], ContStepT[-stepHy * iterationPrediction]])


# Generowanie charakterystyk

ploting(yreferenceHy, yHy, uHy, duHy, stepHy, "Hy", title="Charakterystyki dla różnych wartości Hy",
        iterationPrediction=MaxIterationPrediction)
ploting(yreferenceHu, yHu, uHu, duHu, stepHu, "Hu", title="Charakterystyki dla różnych wartości Hu",
        iterationPrediction=MaxIterationPrediction)
ploting(yreferenceRo, yRo, uRo, duRo, stepRo, "ρ", title="Charakterystyki dla różnych wartości ρ",
        iterationPrediction=MaxIterationPrediction)
ploting(yreferenceControl, yControl, uControl, duControl, 0, "",
        title=f"Charakterystyki sterownika GPC nie uwzględniając ograniczeń układu dla Hy = {ControlHy}, Hu = {ControlHu}, ρ = {ControlRo}",
        iterationPrediction=1)
ploting(yreferenceControlConst, yControlConst, uControlConst, duControlConst, 0, "",
        title=f"Charakterystyki sterownika GPC uwzględniając ograniczenia układu dla Hy = {ControlHy}, Hu = {ControlHu}, ρ = {ControlRo}",
        iterationPrediction=1)
plt.show()
